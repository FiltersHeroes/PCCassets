action.com##+js(bakeCookie, CookieConsent, true, 365)
al.to,combat.pl##+js(bakeCookie, trackingPermissionValueCookie, true, 365)
al.to,combat.pl,x-kom.pl##+js(bakeCookie, trackingPermissionConsentsValue, {%22cookies_analytics%22:true%2C%22cookies_personalization%22:true%2C%22cookies_advertisement%22:true}, 365)
amway.pl##+js(bakeCookie, isAnalyticsCookiesAccepted, false, 365)
amway.pl##+js(bakeCookie, isCookieConsentAccepted, true, 365)
amway.pl##+js(bakeCookie, isMarketingCookiesAccepted, false, 365)
budujmase.pl,drogerium.pl,trec.pl,wylecz.to##+js(bakeCookie, rodoHfM, true, 365)
cineman.pl##+js(bakeCookie, ARE_FUNCTIONAL_COOKIES_ACCEPTED, true, 3650)
cineman.pl##+js(bakeCookie, ARE_MARKETING_COOKIES_ACCEPTED, true, 3650)
cineman.pl##+js(bakeCookie, ARE_REQUIRED_COOKIES_ACCEPTED, true, 3650)
cineman.pl##+js(bakeCookie, HAS_COOKIES_FORM_SHOWED, true, 3650)
electronicbeats.pl##+js(bakeCookie, borlabs-cookie, %7B%22consents%22%3A%7B%22essential%22%3A%5B%22borlabs-cookie%22%2C%22google-tag-manager%22%5D%2C%22statistics%22%3A%5B%22google-analytics%22%5D%2C%22marketing%22%3A%5B%22facebook_share_button%22%2C%22twitter_share_button%22%5D%2C%22external-media%22%3A%5B%22facebook%22%2C%22bandcamp%22%2C%22mixcloud%22%2C%22soundcloud%22%2C%22spotify%22%2C%22googlemaps%22%2C%22instagram%22%2C%22openstreetmap%22%2C%22twitter%22%2C%22vimeo%22%2C%22youtube%22%2C%22externalmedia%22%5D%7D%2C%22domainPath%22%3A%22www.electronicbeats.net%2F%22%2C%22expires%22%3A%22Tue%2C%2006%20Dec%202022%2009%3A36%3A44%20GMT%22%2C%22uid%22%3A%22y8b4cd84-vcupvedk-9sbqhj37-x0h579ap%22%2C%22version%22%3A%223%22%7D, 365)
emuia1.gugik.gov.pl##+js(bakeCookie, cookieMessageHide, true, 365)
espedytor.pl##+js(bakeCookie, zgodaRODO, true, 365)
forgeofempires.com##+js(bakeCookie, CookieNotification, 0, 365)
gamepedia.com,wikia.com,wikia.org##+js(bakeCookie, tracking-opt-in-status, accepted, 365)
gisgminny.pl##+js(bakeCookie, cookiesDirective, 1, 365)
hiszpanskidlapolakow.com##+js(bakeCookie, gdpr-accepted, {"ga":true,"facebook":true,"disqus":true}, 365)
hrkgame.com##+js(bakeCookie, gdpralert, done, 365)
hummel.pl##+js(bakeCookie, CookieInformationConsent, 1, 365)
infor.pl##+js(bakeCookie, inforCookieWallCacheVal, 15, 365)
lawyerka.pl##+js(bakeCookie, ginger-cookie, Y, 365)
laziska.com.pl,m-ce.pl,mojbytom.pl,mojegliwice.pl,mojekatowice.pl,mojmikolow.pl,orzesze.com.pl,piekaryslaskie.com.pl,pyskowice.com.pl,rudaslaska.com.pl,rybnicki.com,siemianowice.net.pl,silesia.info.pl,sosnowiecki.pl,swiony.pl,wodzislaw.com.pl,zabrze.com.pl,zory.com.pl##+js(bakeCookie, rodo, accept, 365)
login.e-dowod.gov.pl##+js(bakeCookie, acceptRodoSie, hide, 365)
mapa.gdansk.gda.pl##+js(bakeCookie, ipg.splash, y, 365)
mapa.gdansk.gda.pl##+js(bakeCookie, splash, y, 365)
mapa.szukacz.pl##+js(bakeCookie, gdprid, 1, 780)
mapy.geoportal.gov.pl##+js(bakeCookie, regulationsAccepted, true, 365)
mapy.geoportal.gov.pl##+js(bakeCookie, rules, 1, 365)
mini.com.pl##+js(bakeCookie, cc_consentCookie, {"mini_poland_family":{"consentMetadataModel":{"analytics":"GIVEN","functional":"GIVEN","advertising":"GIVEN"},"lastModifiedTimestamp":1619619734490,"crossDomainConsent":true}}, 365, .mini.com.pl)
n26.com##+js(bakeCookie, num26GDPR, ACCEPTED, 365)
nike.com##+js(bakeCookie, sq, 3, 365)
parkiet.com,rp.pl##+js(bakeCookie, __gm_tcfconsent_v2, 1, 365)
polsatboxgo.pl,polsatgo.pl##+js(bakeCookie, rodo-modal-displayed, 1, 365)
tumblr.com##+js(bakeCookie, euconsent-v2, CPcxQAAPcxQAAECABAPLCZCgAPLAAHLAAKiQI7Nd_X__bX9n-_7_6ft0eY1f9_r37uQzDhfNs-8F3L_W_LwX32E7NF36tq4KmR4ku1bBIQNtHMnUDUmxaolVrzHsak2cpyNKJ_JkknsZe2dYGF9Pn9lD-YKZ7_5_9_f52T_9_9_-39z3_9f___dv_-__-vjf_599n_v9fV_78_Kf9______-____________8EdgCTDVuIAuzLHBm0DCKFECMKwkKoFABBQDC0QWADg4KdlYBLrCFgAgFSEYEQIMQUYMAgAEEgCQiACQIsEAiAIgEAAIAEQCEADEwCCwAsDAIAAQDQsQAoABAkIMiAiOUwICoEgoJbKxBKCvQ0wgDrPACgURsVAAiSQEUgICQsHAMESAl4skDTFG-QAjBCgFEqAA, 360)
tumblr.com##+js(bakeCookie, euconsent-v2-noniab, AAZG, 360)
tv-trwam.pl##+js(bakeCookie, ARE_FUNCTIONAL_COOKIES_ACCEPTED, true, 365)
tv-trwam.pl##+js(bakeCookie, ARE_MARKETING_COOKIES_ACCEPTED, true, 365)
tv-trwam.pl##+js(bakeCookie, ARE_REQUIRED_COOKIES_ACCEPTED, true, 365)
tv-trwam.pl##+js(bakeCookie, HAS_COOKIES_FORM_SHOWED, true, 365)
ubi2.wit.edu.pl##+js(bakeCookie, GPRD, 128, 365)
vice.com##+js(bakeCookie, _vice_cmp_modal_viewed, true, 365)
virustotal.com##+js(bakeCookie, euConsent, 1, 365)
vivaldi.com##+js(bakeCookie, acceptedCookies, true, 365)
warszawskakranowka.pl##+js(bakeCookie, rodo_warszawskakranowka, 1, 365)
wroclaw.pl##+js(bakeCookie, iRodoInfo, 1, 365)
wunderground.com##+js(bakeCookie, notice_gdpr_prefs, 0,1,2,3:, 365)
wunderground.com##+js(bakeCookie, notice_poptime, 1533920400000, 365)
www2.hm.com##+js(bakeCookie, hm_gdpr_read, true, 365)
